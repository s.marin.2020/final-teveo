# Final-TeVeO

# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Sergio Marín Gutiérrez.
* Titulación: Grado en ingeniería en tecnologías de la telecomunicación.
* Cuenta en laboratorios: marin
* Cuenta URJC: s.marin.2020@alumnos.urjc.es
* Video básico (url): https://www.youtube.com/watch?v=YNuzVt4pyfg
* Video parte opcional (url): https://www.youtube.com/watch?v=x5R95zZWpfo
* Despliegue (url): http://sermarin.pythonanywhere.com
* Contraseñas: admin
* Cuenta Admin Site: marin/admin

## Resumen parte obligatoria

Implementación de los recursos obligatorios. Estos son:
* /'
    El recurso principal, en este podremos ver un listado de los comentarios ordenados de más a menos recientes, además 
estos apareceran paginados. Para cada comentario tenemos la fecha en la que se hizo, el texto del comentario, la imagen 
que se comentó y un enlace a la página de la camara que sacó esa imagen, que será su identificador. 
* '/camaras'
    En este recurso tenemos cuatro botones, los cuales al pulsarlos descargarán la información de distintos archivos de datos, 
si alguno de estos datos no están incluidos en la base de datos esta se actualizará, no haciendo nada en caso de que todo esté 
actualizado. Adicionalmente tenemos una imagen escogida aleatoriamente de entre todas las cámaras de la base de datos, que se 
situará encima de la tabla donde, de forma paginada en función del número de comentarios, podremos ver todas las cámaras almacenadas en la base de datos.
* '/{id_camara}':
    Para este recurso veremos la página estática de la camara, es decir, veremos la imagen de la cámara en el momento 
en el que hicimos la petición, la información de la cámara, el número de likes y un listado de los comentarios ordenados 
en función de su fecha de publicación, de modo que los más recientes aparecerán arriba, además estos están paginados. 
Para esta página también podremos acceder a la página dinámica de la camara, poner comentarios para la imagen que estamos 
viendo y, si el usuario ha guardado una configuración o ha seleccionado un nombre, tendrá la posibilidad de dar un like por 
camara, si este intenta pulsar el botón cuando ya le ha dado like este no hará nada.
* '/{id_camara}-dyn'
    Este recurso es igual que el de la cámara estática, justo el anterior, con la diferencia de que la imagen se irá 
recargando sola cada 30 segundos, al igual que los comentarios.
* '/comentario?id={id_camara}'
    Mediante este recurso puedes poner un comentario en específico para la cámara cuyo identificador coincida con el indicado en la query string.
* '/{id_camara}.json'
    Este recurso aporta un resumen con los datos principales de la cámara cuyo identificador se haya pedido. Este resumen
es el identificador, el lugar, la url de la imagen, las coordenadas, el número de comentarios y el número de likes puestos para esa cámara.
* '/configuracion'
    En esta podrás elegir el nombre con el que quieres que TeVeO te reconozca, el tamaño y la fuente de la letra para 
dar un formato más personalizado a la página. Además puedes eliminar la configuración para que no te reconozca la 
aplicación, o pedir la clave para que copiando la url que te dé puedas transferir la configuración en otro navegador. 

## Lista partes opcionales

* Especial cuidado en el bootstrap
* Paginacion de las listas de cámaras y comentarios
* Funcionalidad de like, solo se puede dar un like si tiene un perfil, y como mucho puedes dar un like por camara.
* Favicon.ico
* Terminar sesión para borrar la configuración
* Obtencion de datos más alla de los obligatorios, parseo dos listados de datos en json para objetener las camaras del Pais Vasco y de Galicia.
