from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Comentario, Perfil


# https://docs.djangoproject.com/en/5.0/topics/forms/modelforms/
class ComentForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['texto']
        labels = {'texto': ''}
        widgets = {
            "texto": forms.Textarea(
                attrs={"cols": 10, "rows": 10, "class": 'form-control', 'placeholder': 'Introduzca aquí su comentario'}),
        }


class UserForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ['nombre', 'letra', 'tamanyo']
        labels = {'nombre': '', 'letra': '', 'tamanyo': ''}
        widgets = {
            'nombre': forms.TextInput(attrs={'placeholder': 'Ingrese su nombre'}),
        }
