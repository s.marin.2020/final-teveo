import json
import random
import string
import urllib.request
from datetime import datetime, timedelta

from django.db.models import Count
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string

from .forms import ComentForm, UserForm
from .models import Camara, Comentario, Like, Perfil
from .parserXml1 import XML_1
from .parserXml2 import XML_2
from .parserJSON import Json_Galicia
from .parserJSON2 import Json_Euskadi


def paginacion(lista, numero, n_pagina):
    paginator = Paginator(lista, numero)
    page_obj = paginator.get_page(n_pagina)
    return page_obj


def get_nombre(request):
    nombre = 'Anónimo'
    if 'cookie_teveo' in request.COOKIES:
        if Perfil.objects.filter(cookie=request.COOKIES['cookie_teveo']):
            nombre = Perfil.objects.filter(cookie=request.COOKIES['cookie_teveo'])[0].nombre
    return nombre


def index(request):
    comentarios_set = Comentario.objects.all().order_by('fecha').reverse()
    contexto = {'nombre': get_nombre(request), 'pagina': paginacion(comentarios_set, 5, request.GET.get('page')),
                'num_cam': Camara.objects.all().count(),
                'num_comment': Comentario.objects.all().count()}
    return render(request, 'teveo/index.html', contexto)


def get_random_image(request):
    random.seed()
    camara_random = Camara.objects.all()[random.randint(0, Camara.objects.all().count() - 1)]
    camara_random.imagen = descargar_img(camara_random.url)
    camara_random.save()
    return HttpResponse(camara_random.imagen, content_type='image/jpeg')


def descargar_img(url):
    cabecera = {
        'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'
    }
    request = urllib.request.Request(url=url, headers=cabecera)
    try:
        with urllib.request.urlopen(request) as response:
            image = response.read()
    except urllib.error.URLError:
        return None
    return image


def camaras(request):
    if request.method == 'POST':
        if request.POST['action'] == 'Listado 1':
            stream = urllib.request.urlopen(
                'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml')
            cameras = XML_1(stream).camaras()
        elif request.POST['action'] == 'Listado 2':
            stream = urllib.request.urlopen(
                'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml')
            cameras = XML_2(stream).camaras()
        elif request.POST['action'] == 'Listado 3':
            cameras = Json_Galicia(
                'https://servizos.meteogalicia.gal/mgrss/observacion/jsonCamaras.action').camaras()
        elif request.POST['action'] == 'Listado 4':
            cameras = Json_Euskadi(
                'https://opendata.euskadi.eus/contenidos/ds_localizaciones/camaras_trafico/opendata/camaras-trafico'
                '.json').camaras()
        if cameras:
            for camara in cameras:

                if not camara['id'] in Camara.objects.values_list('ident',
                                                                  flat=True):
                    Camara(ident=camara['id'], nombre=camara['name'], url=camara['url'].replace(' ', ''),
                           localizacion=camara['coordenadas']).save()
    camaras_populares = Camara.objects.annotate(num_comment=Count('comentario')).order_by('-num_comment')
    contexto = {'nombre': get_nombre(request), 'pagina': paginacion(camaras_populares, 15, request.GET.get('page')),
                'num_cam': Camara.objects.all().count(),
                'num_comment': Comentario.objects.all().count()}
    return render(request, 'teveo/camaras.html', contexto)


def get_coment_dyn(request):
    return comentario(request, 'get_dyn')


def get_coment(request):
    return comentario(request, 'get_camara')


def comentario(request, redireccion):
    try:
        camara = Camara.objects.get(ident=request.GET['id'])
        if request.method == "POST":
            form = ComentForm(request.POST)
            if form.is_valid():
                new_coment = form.save(commit=False)
                new_coment.camara = camara
                new_coment.imagen = camara.imagen
                new_coment.save()
                return redirect(redireccion, id=request.GET['id'])
        return render(request, 'teveo/coment.html',
                      {'nombre': get_nombre(request), 'form': ComentForm, 'camara': camara,
                       'likes': camara.like_set.all().count(),
                       'num_cam': Camara.objects.all().count(),
                       'num_comment': Comentario.objects.all().count()})
    except Camara.DoesNotExist:
        return render(request, 'teveo/error.html',
                      {'nombre': get_nombre(request), 'error_message': 'No existe esa camara para comentarla.',
                       'num_cam': Camara.objects.all().count(),
                       'num_comment': Comentario.objects.all().count()})


def generarCookie():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=30))

def config(request):
    if request.method == 'GET':
        if 'cookie_teveo' not in request.COOKIES:
            redireccion = '/configuracion'
            if 'id' in request.GET:
                redireccion = f'/configuracion?id={request.GET["id"]}'
            response = HttpResponseRedirect(redireccion)
            response.set_cookie(key='cookie_teveo', value=generarCookie(), expires=datetime.now() + timedelta(days=7))
            return response
        perfil = None
        msg = ''
        if Perfil.objects.filter(cookie=request.COOKIES['cookie_teveo']):
            perfil = Perfil.objects.filter(cookie=request.COOKIES['cookie_teveo'])[0]

        if 'id' in request.GET:
            try:
                perfil = Perfil.objects.get(cookie=request.GET['id'])
                response = HttpResponseRedirect('/')
                response.set_cookie(key='cookie_teveo', value=perfil.cookie, expires=datetime.now() + timedelta(days=7))
                return response
            except Perfil.DoesNotExist:
                msg = 'La cookie que está usando para copiar la configuración es errónea.'

        elif 'transferir' in request.GET:
            msg = f'http://localhost:1234{request.path}?id={request.COOKIES["cookie_teveo"]}'

        return render(request, 'teveo/configuracion.html', {
            'nombre': get_nombre(request),
            'form': UserForm(instance=perfil),
            'msg': msg,
            'num_cam': Camara.objects.count(),
            'num_comment': Comentario.objects.count()
        })

    elif request.method == 'POST':
        if request.POST['action'] == 'Guardar':
            form = UserForm(request.POST)
            if form.is_valid():
                try:
                    perfil = Perfil.objects.get(cookie=request.COOKIES['cookie_teveo'])
                    perfil.nombre = form.cleaned_data['nombre']
                    perfil.letra = form.cleaned_data['letra']
                    perfil.tamanyo = form.cleaned_data['tamanyo']
                    perfil.save()
                except Perfil.DoesNotExist:
                    Perfil.objects.create(
                        nombre=form.cleaned_data['nombre'],
                        letra=form.cleaned_data['letra'],
                        tamanyo=form.cleaned_data['tamanyo'],
                        cookie=request.COOKIES['cookie_teveo']
                    )
                return redirect('index')

        elif request.POST['action'] == 'Eliminar':
            cookie = ''
            try:
                perfil = Perfil.objects.get(cookie=request.COOKIES['cookie_teveo'])
                cookie = perfil.cookie
                perfil.delete()
            except Perfil.DoesNotExist:
                pass

            response = HttpResponseRedirect('/')
            response.set_cookie(key='cookie_teveo', value=cookie, expires='Thu, 01 Jan 1970 00:00:00 GMT')
            return response

def get_time(request):
    time = datetime.now().strftime("%Y-%b-%d %H:%M:%S")
    return HttpResponse(time)


def ayuda(request):
    contexto = {'nombre': get_nombre(request),
                'num_cam': Camara.objects.all().count(),
                'num_comment': Comentario.objects.all().count()}
    return render(request, 'teveo/ayuda.html', contexto)


def get_camara(request, id):
    return get_camara_view(request, id, 'teveo/camara.html')


def get_camara_dyn(request, id):
    return get_camara_view(request, id, 'teveo/dynamic.html')


def get_camara_view(request, id, template_name):
    if request.method == 'POST':
        if request.POST.get('action') == 'Me gusta':
            like = Like.objects.filter(camara=Camara.objects.get(ident=id),
                                       user__cookie=request.COOKIES['cookie_teveo'])
            if not like:
                Like.objects.create(camara=Camara.objects.get(ident=id),
                                    user=Perfil.objects.get(cookie=request.COOKIES['cookie_teveo']))

    try:
        cookie = ''
        camara = Camara.objects.get(ident=id)
        comentarios = Comentario.objects.filter(camara__ident=id).order_by('-fecha')
        if 'cookie_teveo' in request.COOKIES:
            cookie = request.COOKIES['cookie_teveo']
        contexto = {
            'nombre': get_nombre(request),
            'camara': camara,
            'pagina': paginacion(comentarios, 3, request.GET.get('page')),
            'num_cam': Camara.objects.count(),
            'num_comment': Comentario.objects.count(),
            'likes': Like.objects.filter(camara=Camara.objects.get(ident=id)).count(),
            'register': Perfil.objects.filter(cookie=cookie)
        }
        return render(request, template_name, contexto)

    except Camara.DoesNotExist:
        return render(request, 'teveo/error.html',
                      {'nombre': get_nombre(request), 'error_message': 'No existe esa camara.',
                       'num_cam': Camara.objects.all().count(),
                       'num_comment': Comentario.objects.all().count()})


def get_css(request):
    letra = 'italic'
    tamanyo = 'normal'
    if 'cookie_teveo' in request.COOKIES:
        if Perfil.objects.filter(cookie=request.COOKIES['cookie_teveo']):
            perfil = Perfil.objects.get(cookie=request.COOKIES['cookie_teveo'])
            letra = perfil.letra
            tamanyo = perfil.tamanyo
    css = render_to_string('teveo/css_perfil.css', {'letra': letra, 'tamanyo': tamanyo})
    return HttpResponse(css, content_type='text/css')


def get_img(request):
    imagen = ''
    if 'idComent' in request.GET:
        imagen = Comentario.objects.get(pk=request.GET.get('idComent')).imagen
    elif 'idImg' in request.GET:
        camara = Camara.objects.get(ident=request.GET.get('idImg'))
        camara.imagen = descargar_img(camara.url)
        imagen = camara.imagen
        camara.save()
    return HttpResponse(imagen, content_type='image/jpeg')


def change_img(request):
    if 'id' in request.GET:
        try:
            print(request.GET.get('id'))
            camara = Camara.objects.get(ident=request.GET.get('id'))
            html_imagen = (f'<img src="/imagen?idImg={camara.ident}&{random.randrange(10000)}" alt ="Imagen de la '
                           f'cámara {camara.ident} ">')
        except Camara.DoesNotExist:
            html_imagen = ''
        return HttpResponse(html_imagen)


def change_comm(request):
    if 'id' in request.GET:
        try:
            comentarios = Comentario.objects.all().filter(camara__ident=request.GET.get('id')).order_by('-fecha')
            contexto = {'comentarios': comentarios}
            return render(request, 'teveo/coment-dyn.html', contexto)
        except Camara.DoesNotExist:
            return render(request, 'teveo/error.html',
                          {'nombre': get_nombre(request), 'error_message': 'No existe esa camara.',
                           'num_cam': Camara.objects.all().count(),
                           'num_comment': Comentario.objects.all().count()})


def get_json(request, id):
    try:
        camara = Camara.objects.get(ident=id)
        camara_dict = {
            'identificador': camara.ident,
            'nombre': camara.nombre,
            'latitud': camara.localizacion.split(',')[1],
            'longitud': camara.localizacion.split(',')[0],
            'url': camara.url,
            'Numero de comentarios': camara.comentario_set.all().count(),
            'Numero de likes': camara.like_set.all().count()
        }
        json_camara = json.dumps(camara_dict, indent=4)
        return HttpResponse(json_camara, content_type='application/json')
    except Camara.DoesNotExist:
        return render(request, 'teveo/error.html',
                      {'nombre': get_nombre(request), 'error_message': 'No existe esa camara.',
                       'num_cam': Camara.objects.all().count(),
                       'num_comment': Comentario.objects.all().count()})
