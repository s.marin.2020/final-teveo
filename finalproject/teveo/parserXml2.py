import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string


class XmlParser(ContentHandler):
    """Class to handle events fired by the SAX parser

    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__(self):
        self.inList = False
        self.inCamara = False
        self.inPlace = False
        self.inContent = False
        self.content = ""
        self.id = ""
        self.url = ""
        self.name = ""
        self.longitude = ""
        self.latitude = ""
        self.camaras = []

    def startElement(self, name, attrs):
        if name == 'list':
            self.inList = True
        elif self.inList:
            if name == 'cam':
                self.inCamara = True
                self.id = attrs.get('id')
            elif self.inCamara:
                if name == 'url':
                    self.inContent = True
                elif name == 'info':
                    self.inContent = True
                elif name == 'place':
                    self.inPlace = True
                elif self.inPlace:
                    if name == 'latitude':
                        self.inContent = True
                    elif name == 'longitude':
                        self.inContent = True

    def endElement(self, name):
        global camaras

        if name == 'list':
            self.inList = False
        elif self.inList:
            if name == 'cam':
                self.inCamara = False
                self.camaras.append({'id': 'LIS2-' + self.id,
                                     'url': self.url,
                                     'name': self.name,
                                     'coordenadas': self.longitude + ',' + self.latitude})
            elif self.inCamara:
                if name == 'url':
                    self.url = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'info':
                    self.name = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'place':
                    self.inPlace = False
                elif self.inPlace:
                    if name == 'latitude':
                        self.latitude = self.content
                        self.content = ""
                        self.inContent = False
                    elif name == 'longitude':
                        self.longitude = self.content
                        self.content = ""
                        self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


class XML_2:
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = XmlParser()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def camaras(self):
        return self.handler.camaras


if __name__ == '__main__':
    stream = urllib.request.urlopen(
        'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml')
    cameras = XML_2(stream.url).camaras()
    for Camara in cameras:
        print(Camara['id'])
