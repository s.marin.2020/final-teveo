import json
import urllib.request


class parseJson:

    def __init__(self, url):
        self.camaras = []
        self.url = url
        self.datos = ''
        self.cleaned_data = ''

    def descargar_datos(self):
        request = urllib.request.Request(url=self.url)
        try:
            with urllib.request.urlopen(request) as response:
                self.datos = response.read().decode('utf-8')
        except urllib.error.URLError:
            self.datos = ''

    def proceso_datos(self):
        self.descargar_datos()
        index_inicio = self.datos.find('(')
        index_final = self.datos.rfind(')')
        if index_final != -1 and index_final != -1:
            self.datos = self.datos[index_inicio + 1:index_final]
            try:
                self.cleaned_data = json.loads(self.datos)
            except json.decoder.JSONDecodeError:
                print('Error al parsear los datos')
        else:
            print('Formato no esperado')

    def guardar_datos(self):
        for camara in self.cleaned_data:
            clean_camara = {'id': 'Euskadi-' + camara['INDEX'], 'name': camara['TITLE'], 'url': camara['URLCAM'],
                            'coordenadas': camara['LONWGS84'] + ',' + camara['LATWGS84']}
            self.camaras.append(clean_camara)

class Json_Euskadi:

    def __init__(self, stream):
        self.parser = parseJson(stream)
        self.parser.proceso_datos()
        self.parser.guardar_datos()

    def camaras(self):
        return self.parser.camaras


if __name__ == '__main__':
    JSON = Json_Euskadi(
        'https://opendata.euskadi.eus/contenidos/ds_localizaciones/camaras_trafico/opendata/camaras-trafico.json')
    JSON.camaras()
