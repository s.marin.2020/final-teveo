import json
import urllib.request


class parseJson:

    def __init__(self, url):
        self.camaras = []
        self.url = url
        self.datos = ''
        self.cleaned_data = ''

    def descargar_datos(self):
        request = urllib.request.Request(url=self.url)
        try:
            with urllib.request.urlopen(request) as response:
                self.datos = response.read().decode('utf-8')
        except urllib.error.URLError:
            self.datos = ''

    def proceso_datos(self):
        self.descargar_datos()
        try:
            self.cleaned_data = json.loads(self.datos)['listaCamaras']
        except json.decoder.JSONDecodeError:
            print('Error al parsear los datos')

    def guardar_datos(self):
        for camara in self.cleaned_data:
            clean_camara = {'id': 'Gal-' + str(self.cleaned_data.index(camara)), 'name': camara['nomeCamara'], 'url': camara['imaxeCamara'],
                            'coordenadas': str(camara['lon']) + ',' + str(camara['lat'])}
            self.camaras.append(clean_camara)
class Json_Galicia:

    def __init__(self, stream):
        self.parser = parseJson(stream)
        self.parser.proceso_datos()
        self.parser.guardar_datos()

    def camaras(self):
        return self.parser.camaras


if __name__ == '__main__':
    JSON = Json_Galicia(
        'https://servizos.meteogalicia.gal/mgrss/observacion/jsonCamaras.action')
    JSON.camaras()
