from django.contrib import admin
from .models import Camara, Comentario, Perfil, Like
admin.site.register(Camara)
admin.site.register(Comentario)
admin.site.register(Like)
admin.site.register(Perfil)